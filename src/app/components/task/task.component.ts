import { Component, Input } from '@angular/core';

import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ITask } from 'src/app/core/interfaces/ITask';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {MatIconModule} from '@angular/material/icon';
import { TaskServiceService } from 'src/app/core/services/task-service.service';

@Component({
  selector: 'app-task',
  standalone: true,
  imports: [CommonModule, MatCheckboxModule, MatIconModule, FormsModule],
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent {
  @Input() task : ITask | null = null;
  @Input() idx : number = -1;

  constructor (
    private taskService: TaskServiceService
  ){}
  removeItem():void{
    if(this.idx < 0) {
      alert("El elemento que desea eliminar no existe")
      return
    }
    this.taskService.removeTask(this.idx)
  }
}
