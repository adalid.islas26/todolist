import { ITask } from '../interfaces/ITask';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TaskServiceService {
  private listTask : ITask[] = []
  
  constructor() { }

  addTask(task : string) : void {
    this.listTask.push({
      task: task,
      status: false,
      create : new Date()
    })
  }

  removeTask(idx:number) : void {
    this.listTask.splice(idx, 1)
  }

  getTotalTask(): number {
    return this.listTask.length
  }

  getTtotalTaskEnded() : number {
    return this.listTask.filter(e => e.status).length;
  }

  getListTask() : ITask[]{
    return this.listTask
  }
}
